# Setup

## Step 0: Homebrew

The first thing you'll need, if using a mac, is to install [Homebrew](https://brew.sh). It's recommended to use it to install and manage all required packages.

## Step 1: Elixir

The easiest way to install elixir is via Homebrew. Just run `brew install elixir` to install the latest version.

## Step 2: Node

Use Homebrew to also install the latest *LTS* version of [Node](https://nodejs.org/en/).

First, check which is the latest LTS version [here](https://nodejs.org/en/). Then, use the command `brew install node@VERSION` to install it. Example:

```bash
brew install node@10
```

## Step 3: Yarn

For JavaScript dependencies management, install [yarn](https://yarnpkg.com/docs/install/#mac-stable):

```bash
brew install yarn --ignore-dependencies
```

## Step 4: PostgreSQL

Hopefully, this will be as easy as running

```bash
brew install postgresql
```

It's fine to use the default credentials `postgres/postgres` here.

And then, to start postgres:

```bash
brew services start postgresql
```

And create an user:

```bash
createuser -s postgres
```

## Step 5: Running a project

You should be ready to run a project locally now! When running a project for the first time, you will need to setup the database for it.

The initial steps usually are:

1. Go to the **config/** directory, make a copy of the file **NAME.secret.example.exs** and name it **NAME.secret.exs** (Where NAME will usually be **db** or **dev**).
2. In this newly created file, put your PostrgreSQL credentials. If you are using the default, you wont't have to change anything here.
3. Run `mix deps.get` to install the backend dependencies.
4. Run `yarn` to install the frontend dependencies.
5. Run `mix ecto.setup` to create the development database and run the seeds.

Now that everything is setup, you should be able to run the project with

```bash
mix phx.server
```

Or, if you will be changing some code in the backend and want to be able to easily recompile it

```bash
iex -S mix phx.server
```

The command `mix ecto.reset` is also useful to reset the database and run the seeds again.

## Possible problems

If you run into any errors while trying to run the project on step 5, try looking for a solution below.

```
  BCrypt error: [error] Process #PID<0.419.0> raised an exception ** (RuntimeError) An error occurred when loading Bcrypt. Make sure you have a C compiler and Erlang 20 installed. If you are not using Erlang 20, either upgrade to Erlang 20 or use version 0.12 of bcrypt_elixir. See the Comeonin wiki for more information. (bcrypt_elixir) lib/bcrypt/base.ex:17: Bcrypt.Base.init/0 (kernel) code_server.erl:1340: anonymous fn/1 in :code_server.handle_on_load/5
```

Solution: run `mix deps.compile` before running `mix ecto.setup`.
