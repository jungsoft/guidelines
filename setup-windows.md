# Setup

## Step 1: Install and Configure WSL

- WSL is an optional feature available in Windows 10 that allows you to run Linux binaries and scripts directly on Windows.
- Follow [this guide](https://docs.microsoft.com/en-us/windows/wsl/install-win10) to setup WSL.
- Use [Remote - WSL](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl)
extension lets you use VS Code on Windows to build applications that run on the WSL.

`Note: Preferably choose the Debian distros. In the next steps you will use the WSL terminal.`

## Step 1.1: Config Limits VM memory

- In the `C:\Users\Name` root of your user, create a file name `.wslconfig` and put this code:

    ```
    [wsl2]
    memory=4GB # Limits VM memory in WSL 2 to 4 GB
    ```

## Step optional: Install and Configure asdf

- [Asdf](https://asdf-vm.com/) is a version management tool.
- You can decide to use the default installation or use asdf installation.
- Before the guide, you need to run in the WSL terminal:

    ```bash
    sudo apt-get install software-properties-common
    sudo apt-get install gnupg-agent
    sudo apt-get update && sudo apt-get install build-essential
    ```

- Follow [this guide](http://asdf-vm.com/guide/getting-started.html#_1-install-dependencies) to setup asdf.

`Note: Download asdf by git, install asdf by Bash & Git.`

## Step 2: Node + Yarn

- Default installation: 
  - Follow [this guide](https://nodejs.org/pt-br/download/package-manager/) to setup nodejs.
  - Follow [this guide](https://classic.yarnpkg.com/en/docs/install) to setup yarn.
- Asdf installation: 
  - Follow [this guide](https://github.com/asdf-vm/asdf-nodejs/) to setup nodejs.
  - Follow [this guide](https://github.com/twuni/asdf-yarn) to setup yarn.

`Note: Make sure that you install the LTS version of node.`
## Step 3: Erlang + Elixir

- Default installation: 
  - Follow [this guide](https://elixir-lang.org/install.html#gnulinux) to setup Elixir and Erlang.
- Asdf installation: 
  - Follow [this guide](https://github.com/asdf-vm/asdf-erlang) to setup Erlang.
  - Follow [this guide](https://github.com/asdf-vm/asdf-elixir) to setup Elixir.

`Note: Install the latest versions, the only prerequisite for Elixir is Erlang, version 22.0 or later. When you use the default installation, installing Elixir, Erlang is usually automatically installed for you.`

## Step 4: PostgreSQL

- Default installation: 
  - Follow [this guide](https://www.postgresql.org/download/linux/ubuntu/) to setup PostgreSQL.
- Asdf installation:
  - Follow [this guide](https://github.com/smashedtoatoms/asdf-postgres) to setup PostgreSQL.

`Optional:` Then if you need to change the default user (postgres) password

```bash
sudo su postgres

psql
ALTER USER postgres WITH PASSWORD 'new_password';
```

`Note: Install the latest versions.`

## Step 5: Configure Git

Then configure your username and email:

```bash
git config --global user.name "Your Name"
git config --global user.email "Your email"
```

To make git persist your credentials, follow [this guide](https://help.github.com/en/articles/adding-a-new-ssh-key-to-your-github-account) to setup your SSH keys.

Alternatively, if you don't want to setup SSH keys, you can use `git store`:

`Note: This will store your password in plain text on the WSL.`

```bash
git config --global credential.helper store
git pull
<input your credentials this time>
```

## Step 6: Running a project

Read the project README and follow its intructions. 🚀

## Recommended software

[Windows terminal](https://www.microsoft.com/pt-br/p/windows-terminal/9n0dx20hk701?activetab=pivot:overviewtab): It can run any command line application, including all Windows terminal emulators, in a separate tab.