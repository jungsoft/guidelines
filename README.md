<img src="./images/jungsoft_logo.png" width="300px"/>

# Guidelines

**Welcome to Jungsoft's development guidelines!**

This set of general rules and guidelines must be followed in every project, unless a particular project specifies different guidelines to be followed.

If you are just getting started, you may want to look at the setup guide to get up and running:
- [Setup for Mac OS X](setup.md)
- [Setup for Linux](setup-linux.md)
- [Setup for Windows](setup-windows.md)

1. [Workflow](#workflow)
1. [Time Tracking](#time-tracking)
1. [Git Guidelines](#git-guidelines)
1. [Style Guide](#style-guide)
1. [Code Review](#code-review)
1. [Development Tips](#development-tips)
1. [VSCode Configuration](#vscode-configuration)
1. [UNIX Tools](#unix-tools)

# Workflow

To avoid trouble, always follow this workflow:

1. Create an issue or assign yourself to an existing issue
2. Start tracking time for the issue
3. Create a Merge Request with Draft status (can be done through the GitLab UI), this will create a new branch
4. Go to the created branch

    ```
    git checkout <BRANCH>
    ```

5. Commit your work **often**, following the guidelines
6. Once done, test your code thoroughly
7. Go to the Merge Request and review your code! (and make sure the pipeline is passing)
8. Remove Draft status of Merge Request
9. Move the issue to Code Review in the board
10. Ask for someone to review your code and stop tracking time
11. After receiving discussions, start tracking time in the issue again and solve the discussions
12. Let the reviewer know that you are done
13. Done! Your code will probably be running in production soon 😄

**[⬆ back to top](#guidelines)**

# Time Tracking

It's essential to track your time correctly, since this is used to evaluate your performance, manage the team and bill our clients.

*(Just by using the [TMetric browser extension](https://chrome.google.com/webstore/detail/tmetric-%E2%80%93-time-tracker-pr/ffijoclmniipjbhecddgkfpdafpbdnen) you'll already be following most rules.)*

Note that there are **several types of time tracking**, which need to be respected:

- Development: time spent on actual development, writing code or searching for solutions and testing.
- Code Review
- Onboarding / Company General Meetings / 1:1s /  General Internal Discussions
- Internal Meetings (discussions on issues, helping a teammate, etc)
- Client Meetings (project planning, support, clarifications, etc)
- Management (issue planning, issue review, etc)

These affect which tags you should use and where you should track your time, as described below.

## Basic rules

Always follow these basic rules:

* If you need to write a description, keep it clear and objective.

  ❌  BAD:

  ```
  Fix bugs
  ```

  ✅ GOOD:

  ```
  Fix withdraw button color
  ````

* Do not agglomerate issues or Merge Requests in a single entry

  ❌  BAD:

  ```
  #123, #124, #128
  ```

  ✅ GOOD:

  ```
  #123 Fix withdraw button color
  ````

* When mentioning an issue, do not write only the issue number. Include more details

  ❌  BAD:

  ```
  #123.
  ```

  ✅ GOOD:

  ```
  #123 Fix withdraw button color
  ````

* Pay attention to assigning the correct Project and Tag

* Make sure to always have a **Work Type** for Tags (the ones that start with a `$`) - You can remove the other tags generated automatically by TMetric, they aren't useful

  ❌  BAD:

  <img src="./images/time_tracking_bad.png"/>

  ✅ GOOD:

  <img src="./images/time_tracking_good.png"/>

* Track only working time! Stop tracking when going for lunch, açaí, or taking a break.

## Time Tracking - Development

### Applies for

- Writing code
- Fixing bugs
- Resolving discussions
- Testing the code that you wrote
- Planning on how to solve the issue that you are working on
- Reviewing the code that you wrote before asking for an official review

### How to track

- **Project**: **client** projects or **Internal** only if working with CJA.

- **Always track this time in the issue**

- Always use the `$ Development` tag (note the billing icon - `$`)

- If you judge necessary, you can describe the activity on the `Notes` field.
  - E.g. `Fixing a bug that has nothing to do with the issue but is blocking me`

## Time Tracking - Code Review

### Applies for

- Reviewing code written by a teammate

### How to track

- **Project**: **client** projects or **Internal** only if working with CJA.

- **Always track this time in the merge request**

- Always use the `$ Code-Review` tag (note the billing icon - `$`)

## Time Tracking - Onboarding / Company General Meetings / 1:1s /  General Internal Discussions

## Applies for

- Onboarding meetings with team members
- Periodic meetings with company

### How to track

- **Project**: Only **Internal** should be used.

- There will always be a task on TMetric, track your time there

- Always use the `$ Meeting Internal` tag (note the billing icon - `$`)

## Time Tracking - Internal Project Meetings

### Applies for

- Periodic meetings with team members on one project
- Sprint meetings with team members on one project
- A meeting where you are discussing on how to implement the issue
- A meeting where you are receiving help of a teammate on the issue
- A meeting where you are helping a teammate on the issue
- A meeting where you are clarifying things about the issue

### How to track

- **Project**: Only **client** projects should be used.

- **Always track this time in the issue**

- Always use the `$ Meeting Internal` tag (note the billing icon - `$`)

## Time Tracking - Client Meetings

### Applies for

- A meeting where you are presenting new features to the client
- A meeting where you are supporting the client on an issue
- A meeting where you are planning features with the client
- A meeting where you are clarifying things with the client

### How to track

- **Project**: Only **client** projects should be used.

- If possible, track you time on the Monday pulse related to the subject

- If the above is not possible, create a task on TMetric and track your time there

- Always use the `$ Meeting Client` tag (note the billing icon - `$`)

## Time Tracking - Management

### Applies for

- Reviewing issues that are located in `Planned`
- Planning sprints for projects
- Planning features for projects

### How to track

- **Project**: Mainly **client** projects should be used. When related to CJA or the office, then **Internal**.

- When reviewing issues, track your time **on the issue**

- When managing other subjects, if possible, track you time on the Monday pulse related to the subject

- If the above is not possible, create a task on TMetric and track your time there

- Always use the `$ Management` tag (note the billing icon - `$`)

**[⬆ back to top](#guidelines)**

# Git Guidelines

The following basic rules are based on https://github.com/trein/dev-best-practices/wiki/Git-Commit-Best-Practices.

## Basic Rules

### Commit Related Changes

A commit should be a wrapper for related changes. For example, fixing two different bugs should produce two separate commits. Small commits make it easier for other developers to understand the changes and roll them back if something went wrong. With tools like the staging area and the ability to stage only parts of a file, Git makes it easy to create very granular commits.

### Commit Often

Committing often keeps your commits small and, again, helps you commit only related changes. Moreover, it allows you to share your code more frequently with others. That way it‘s easier for everyone to integrate changes regularly and avoid having merge conflicts. Having large commits and sharing them infrequently, in contrast, makes it hard to solve conflicts.

### Don't Commit Half-Done Work

You should only commit code when a logical component is completed. Split a feature‘s implementation into logical chunks that can be completed quickly so that you can commit often. If you‘re tempted to commit just because you need a clean working copy (to check out a branch, pull in changes, etc.) consider using Git‘s **Stash** feature instead.

### Test Your Code Before You Commit

Resist the temptation to commit something that you **think** is completed. Test it thoroughly to make sure it really is completed and has no side effects (as far as one can tell). While committing half-baked things in your local repository only requires you to forgive yourself, having your code tested is even more important when it comes to pushing/sharing your code with others.

### Commit message

It is critically important to write good commit messages. It will help everyone (including your future self) understand the purpose of each commit and how the project evolves over time. [(Further reading)](https://chris.beams.io/posts/git-commit/).

For your commit message, follow these rules:

1. Capitalize the subject line

    This is a simple rule that seeks to standardize all commit messages.

    ❌  BAD:

    ```
    fix something
    ```

    ✅ GOOD:

    ```
    Fix something
    ````

2. Use the imperative mode

      This rule is also important in order to standardize all commit messages.

      Git itself uses the imperative whenever it creates a commit on your behalf, so follow the given convention.

      ❌  BAD:

      ```
      Fixed something
      Fixing something
      ```

      ✅ GOOD:

      ```
      Fix something
      ```

3. Provide a short summary covering ALL the changes made in the commit

    This means that anyone in the project must be able to understand the purpose of your commit. Don't expect any prior knowledge from the reader.

    Remember that commits will all end up in the master branch, so don't assume knowledge about the Merge Request and Issue associated with a commit. The commit message should contain all the necessary information.

    The following bad examples were taken from our repositories :)

    ❌  BAD:

    ```bash
    Added query  # what query?

    Fix bug # what bug?

    First table done # what table? (this also doesn't follow the imperative mode rule)

    Added some information to table # What information? What table?

    Minor fix # What was fixed? How?

    Add missing queries # Which queries?

    Made some changes as discussed # What if I wasn't involved in that discussion? Even if I was, Am I expected to remember what was discussed in the day this commit was made?

    Resolve discussions # Same as above

    Improve parts of the code # How? Which parts?

    All done # You get the idea now, right?
    ```

    ✅ GOOD:

    ```
    Add X query to Y
    Fix bug X by doing Y
    Create X table
    Add X to table Y
    Refactor X
    ```

## Learn Git

Sometimes the commit messages are bad simply because the commits are bad. Learn to use at least the [git amend](https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things) command.

❌  BAD:

```
Fix something
Fix something again, that did not work

Some commit
Fix typo
Fix lint
Fix tests
```

✅ GOOD:

```
Fix something for good

Some complete commit
```

Another really powerful command is [git rebase](https://git-scm.com/docs/git-rebase). With it, you can [squash commits](https://stackoverflow.com/questions/5189560/squash-my-last-x-commits-together-using-git), amend older commits and even merge more gracefully.

[Learn more about the differences between merge and rebase here.](https://medium.freecodecamp.org/an-introduction-to-git-merge-and-rebase-what-they-are-and-how-to-use-them-131b863785f)

## Solving merge conflicts

Use `git rebase` to solve conflicts. For example, let's assume your branch has conflicts with the latest master and can't be automatically merged.

The first step is to create a backup in case something goes wrong:

```
git checkout <your-branch>
git branch backup
```

Then, if something goes horribly wrong (don't worry, it *usually* doesn't) just do `git reset --hard backup`.

Now, to perform the rebase:

```
git checkout master
git pull
git checkout <your-branch>
git rebase master
```

Or simply

```
git checkout <your-branch>
git pull --rebase origin master
```

Then, git will walk through the conflicts commit-by-commit, allowing you to solve them. After solving each conflict do

```
git add -A # Or add each file separately to be safe
git rebase --continue
```

 ⚠️ It is very important to pay attention to rebases, since they have the potential to break or erase existing code! If in doubt, go to the `master` branch and see what the state was before the rebase, or ask the person whose work conflicted with yours.  ⚠️

If you got to this stage, your **local** branch is now rebased with the master branch. The last step is to update your **remote** branch with your **local** branch. Since the rebase process may alter your **local** branch structure, a simple `git push` may not do the job, since git won't know how to handle the difference in structure between your **local** branch and your **remote** branch. The solution is to run the following command, which tells git to overwrite your **remote** branch with your **local** branch:

```
git push -f
```

## Git hooks

Sometimes we forget to run the tests or the lint before pushing our work, then the pipeline fails and we don't even see it. This makes it impossible to merge our work and wastes everyone's time. [Git hooks](https://githooks.com/) can help us with this problem.

Create the file **~/.git/hooks/pre-push** (you may need to create these folders):

```bash
# Define colors
RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[0;35m'

printf "${GREEN}[Git pre-push hook] Running tests before push...\n${NC}"
if [ -e ./.git/hooks/pre-push ]; then # If the project defines a pre-push, use it instead of this one
    ./.git/hooks/pre-push "$@"
else
  if mix credo --strict && mix test ; then # Insert here the command to run before every push (e.g. yarn lint)
    exit 0;
  else
    printf "\n${RED}Aborted: ${NC} Lint or tests failed!\n"
    exit 1
  fi
fi
```

And make the file executable: `chmod +x pre-push`.

Now, configure your git to use that location: `git config --global core.hooksPath '~/.git/hooks'`.

If your project requires different commands to be run before push, you can create a custom pre-push at **your_project/.git/hooks/pre-push** (don't forget to make it executable with the command above):

```bash
# Define colors
RED='\033[0;31m'
NC='\033[0m'

if mix credo --strict && mix unauth_test ; then
  exit 0;
else
  printf "${RED}Aborted: ${NC} Lint or tests failed!\n"
  exit 1
fi
```

*Tip: to push without running hooks do `git push --no-verify`*

**[⬆ back to top](#guidelines)**

# Style Guide

Follows these style guides to keep your code consistent and readable.

## Elixir

https://github.com/jungsoft/elixir-style-guide

## Javascript

https://github.com/airbnb/javascript

https://github.com/airbnb/javascript/tree/master/react

## Frontend

https://github.com/jungsoft/frontend-style-guide

**[⬆ back to top](#guidelines)**

# Code Review

Best practices for both reviewers and reviewees!

## Purpose

It's important to understand the purpose of code reviews. The focus is not just detecting bugs, but also transferring knowledge, increasing team awareness and improving code quality.

The purpose is not only to have better code, but also better developers!

## Best practices for authors

* When creating a Merge Request, try your best to follow the team's best practices and remember past discussions to **avoid repeating the same errors**. You are also encouraged to create discussions in your own MR, asking for different opinions or providing clarifications when you judge necessary.

* When solving discussions, feel free to discuss them! The idea is not to follow blindly whatever the reviewer wrote, but understand the idea behind a proposed change or disagree with the change and explain your reasoning.

* When it's time to actually change the code, create new commits (do not use `git amend`) so the reviewer can later see how you solved the discussion. Also remember the commit messages best practices and do not create a commit with a message like "Resolve discussions"!

* **Do not apply suggestions**. Suggestions are really, really amazing! They provide an easy-to-view diff on the discussion and directly shows the point that the reviewer intended to show. However, note that it is really dangerous to apply suggestions without thinking. Suggestions only affect a few lines and the reviewer usually does not have access to the entire file at the moment of the discussion. Another point is that the commit message they provide isn't helpful at all. Example:

<img src="./images/suggestion_bad.png"/>

* If you have made any changes to the interface (frontend development), it's a really nice idea to add a comment on your merge request with a screenshot of what has changed, possibly adding a before / after comparison. **This really makes the life easier for reviewers**, as they are able to know what has changed visually just by opening the MR. Example:

<img src="./images/cr_implementation_styles.png"/>

* If you had to refactor, rewrite or change something complicated in the code, it's also a good idea to document what you have changed with a comment in your MR. Again, this makes it much easier on the reviewer to understand what you meant by the code you wrote. Example:

<img src="./images/cr_implementation_details.png"/>

## Best practice for reviewers

* When reviewing someone's code, avoid assuming they're wrong or stupid, remember that everyone is actually trying their best. Try to ask for clarifications instead of pointing out that something is wrong.
* Try to not only point out problems, but also offer positive feedback when you see something good.
* Insist on high quality reviews but agree to disagree. People have different opinions, do not push yours when it's not that relevant.
* Quote the style guide. Preferably, all code style should be automatically checked by the linter, when that's not possible, quote the style guide so the author can better understand the reasoning. Most of the time, if the linter didn't complain and the rule is not in the style guide, it's just your personal opinion.

# Development Tips

Here are some tips to help you during your development tasks.

## Document problems that you had

If you're working on a merge request and find yourself blocked by any kind of problem (e.g. Apollo client cache wasn't working properly), it's a really good idea to document this with a comment in your merge request. This will let the reviewer plus anyone who's looking at the MR immediately know that you dealt with this problem there.

You can include links that you used to research the problem, screenshots, GitHub / GitLab issues, demos, it's all about adding as much information as possible while still being efficient.

And of course, if you feel like you're stuck because of this problem you found, it's always a good idea to ask others for help.

**[⬆ back to top](#guidelines)**

# VSCode Configuration

To open your VSCode settings, do (CTRL/CMD + P) and type `>Preferences: Open Settings (JSON)`.

## Essential settings

These settings must be used by everyone to ensure consistency:

```json
{
  "editor.tabSize": 2,
  "files.trimTrailingWhitespace": true,
  "files.insertFinalNewline": true,
  "files.trimFinalNewlines": true,
}
```

## Helpful settings

These settings are not required but may improve the developing experience:

```json
{
    "window.zoomLevel": 0,
    "git.enableSmartCommit": true,
    "search.exclude": {
        "**/node_modules": true,
        "**/bower_components": true,
        "**/tmp": true,
        "**/_build": true,
        "**/deps": true
    },
    "editor.minimap.enabled": false,
    "breadcrumbs.enabled": true,
    "files.autoSave": "onFocusChange",
}
```

## Extensions

Recommended extensions (feel free to add your suggestions here!):

* [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)

  This extension will allow you to see who wrote each line of code without leaving the code editor. These settings are recommended:

  ```json
  {
      "gitlens.advanced.messages": {
          "suppressCommitHasNoPreviousCommitWarning": false,
          "suppressCommitNotFoundWarning": false,
          "suppressFileNotUnderSourceControlWarning": false,
          "suppressGitVersionWarning": false,
          "suppressLineUncommittedWarning": false,
          "suppressNoRepositoryWarning": false,
          "suppressResultsExplorerNotice": true,
          "suppressShowKeyBindingsNotice": true,
          "suppressUpdateNotice": false,
          "suppressWelcomeNotice": true
      },
      "gitlens.keymap": "chorded",
      "gitlens.views.fileHistory.enabled": true,
      "gitlens.views.lineHistory.enabled": true,
  }
  ```

* [Material Theme](https://marketplace.visualstudio.com/items?itemName=Equinusocio.vsc-material-theme)

  Material Theme Darker High Contrast is my recommendation :)

* [Highlight Bad Chars](https://marketplace.visualstudio.com/items?itemName=wengerk.highlight-bad-chars)

  For Mac users.

* [ElixirLS](https://marketplace.visualstudio.com/items?itemName=JakeBecker.elixir-ls)

  For elixir developers.

* [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

  For Javascript developers.

* [Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)

  To avoid typos in your code.

**[⬆ back to top](#guidelines)**

# UNIX Tools

Helpful tools for Linux and macOS

* [oh-my-git](https://github.com/arialdomartini/oh-my-git)
* [autojump](https://github.com/wting/autojump)

**[⬆ back to top](#guidelines)**
