# Setup

## Step 0: Elixir

Follow [this guide](https://elixir-lang.org/install.html#gnulinux) according to your distribution.

## Step 1: Node + Yarn

Make sure that you install the LTS version of node, following [this guide](https://nodejs.org/pt-br/download/package-manager/).

Then, follow [this guide](https://classic.yarnpkg.com/en/docs/install) to setup yarn.

## Step 2: PostgreSQL

To install PostgreSQL version 12 simply run:

```
sudo apt install postgresql-12
```

Then if you need to change the default user (postgres) password:

```
sudo su postgres

psql
ALTER USER postgres WITH PASSWORD 'new_password';
```

## Step 3: Install Git

To install, simply run:

```
sudo apt install git
```

Then configure your username and email:

```
git config --global user.name "Your Name"
git config --global user.email "Your email"
```

To make git persist your credentials, follow [this guide](https://help.github.com/en/articles/adding-a-new-ssh-key-to-your-github-account) to setup your SSH keys.

Alternatively, if you don't want to setup SSH keys, you can use `git store`:

`WARNING: This will store your password in plain text on the machine.`

```
git config --global credential.helper store
git pull
<input your credentials this time>
```

## Step 4: Running a project

You should be ready to run a project locally now! When running a project for the first time, you will need to setup the database for it.

The initial steps usually are:

1. Go to the **config/** directory, make a copy of the file **NAME.secret.example.exs** and name it **NAME.secret.exs** (Where NAME will usually be **db** or **dev**).
2. In this newly created file, put your PostrgreSQL credentials. If you are using the default, you wont't have to change anything here.
3. Run `mix deps.get` to install the backend dependencies.
4. Run `yarn` to install the frontend dependencies.
5. Run `mix ecto.setup` to create the development database and run the seeds.

Now that everything is setup, you should be able to run the project with

```bash
mix phx.server
```

Or, if you will be changing some code in the backend and want to be able to easily recompile it

```bash
iex -S mix phx.server
```

The command `mix ecto.reset` is also useful to reset the database and run the seeds again.


## Recommended software

[Powerline](https://powerline.readthedocs.io/en/master/installation/linux.html): You can use powerline to customize your terminal experience.
Follow [this guide](https://www.dobitaobyte.com.br/o-poder-do-powerline-no-shell/) to install it.

[Tilix](https://gnunn1.github.io/tilix-web/): An improved terminal app.
To install it:
```
sudo apt install tilix
```

[Snap store](https://snapcraft.io/store): Allows you to install all software (e.g. VSCode, Slack) from one store and receive automatic updates.
To install it:
```
sudo apt install snapd
sudo snap install snap-store
```
